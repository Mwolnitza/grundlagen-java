package String1;

public class Stringuebung {
    public static void main(String[] args) {
        String sl1 = "Hallo";
        String sl2 = "Hallo";
        String sk1 = new String("Hallo");
        String sk2 = new String("Hallo");
        System.out.println(sl1 == sl2); //true
        System.out.println(sk1 == sk2); //flase
        System.out.println(sl1 == sk1); //false
        System.out.println(sl1.equals(sk1)); //true
    }
}