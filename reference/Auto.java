package reference;

import java.util.Objects;

public class Auto {

    public static void main(String[]args){
/*
        String test1 =  new String("Hallo1");
        String test2 =  new String("Hallo");
        String test3 = test2;

        System.out.println(test2 == test1);     //false
        System.out.println(test2 == test3);  // True
        System.out.println(test1.equals(test2));  // true

        test3 = "Hallo2";
        System.out.println(test3); // Hallo | Hallo2

 */
        Auto1 auto1 =  new Auto1();
        Auto1 auto2 =  new Auto1();
        Auto1 auto3 = auto2;

        System.out.println(auto1 == auto2);     //false
        System.out.println(auto2 == auto3);  // True

        auto3.gewicht = 0;
        System.out.println(auto2.gewicht); // 0

    }
}
